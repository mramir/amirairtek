﻿using System.Collections.Generic;

namespace amir_air_tek.Data
{
    public class FlightSchedule
    {
        public FlightSchedule()
        {
            this.FlightNumber = string.Empty;
            this.Departure = string.Empty;
            this.Destination = string.Empty;
            this.Day = 0;
            this.Capacity = 20;
            this.OrderCount = 0;
        }

        public string FlightNumber { get; set; }
        public string Departure { get; set; }
        public string Destination { get; set; }
        public int Day { get; set; }
        public int Capacity { get; set; }
        public int OrderCount { get; set; }
    }
}
