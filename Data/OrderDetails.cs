﻿namespace amir_air_tek.Data
{
    public class OrderDetails
    {
        // The only reason this class exists is because of the structure of the input json file given to me
        // otherwise, there would be no need for it. I only use it to deserialize the json string.
        public string Destination { get; set; }
    }
}
