﻿using System;
namespace amir_air_tek.Data
{
    public class Order
    {
        public Order()
        {
            this.Name = string.Empty;
            this.Flight = null;
        }

        public string Name { get; set; }
        public FlightSchedule Flight { get; set; }
    }
}
