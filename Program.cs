﻿using System;
using amir_air_tek.Managers;

namespace amir_air_tek
{
    class Program
    {
        static void Main(string[] args)
        {
            var scheduleManager = new ScheduleManager();

            // Story #1
            var flightSchedules = scheduleManager.FlightSchedules;
            foreach(var flight in flightSchedules)
            {
                Console.WriteLine($"Flight: {flight.FlightNumber}, departure: {flight.Departure}, arrival: {flight.Destination}, day: {flight.Day}");
            }

            // Story #2
            var orders = scheduleManager.Orders;
            foreach(var order in orders)
            {
                var flightDetails = order.Flight == null ? "not scheduled" :
                    $"{order.Flight.FlightNumber}, departure: {order.Flight.Departure}, arrival: {order.Flight.Destination}, day: {order.Flight.Day}";
                Console.WriteLine($"order: {order.Name}, flightNumber: {flightDetails}");
            }

            Console.ReadLine();
        }
    }
}
