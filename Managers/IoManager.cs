﻿using System;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace amir_air_tek.Managers
{
    public static class IoManager
    {
        public static T ReadFileAndParse<T>(string path)
        {
            var fileContents = ReadFileContents(path);
            var output = ParseJsonObject<T>(fileContents);
            return output;
        }
        
        private static T ParseJsonObject<T>(string input)
        {
            var serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            return JsonConvert.DeserializeObject<T>(input, serializerSettings);
        }

        private static string ReadFileContents(string path)
        {
            using (StreamReader r = new StreamReader(path))
            {
                string fileContents = r.ReadToEnd();
                return fileContents;
            }
        }
    }
}
