﻿using System;
using System.Collections.Generic;
using System.Linq;
using amir_air_tek.Data;

namespace amir_air_tek.Managers
{
    public class ScheduleManager
    {
        public List<FlightSchedule> FlightSchedules { get; set; }
        public List<Order> Orders { get; set; } = new List<Order>();

        public ScheduleManager()
        {
            this.FlightSchedules = IoManager.ReadFileAndParse<List<FlightSchedule>>("json/flight-schedule.json");

            this.PlaceOrdersInFlights();
            
        }

        private void PlaceOrdersInFlights()
        {
            var ordersFromInput = IoManager.ReadFileAndParse<Dictionary<string, OrderDetails>>("json/coding-assigment-orders.json");
            foreach(var item in ordersFromInput)
            {
                var flight = this.FlightSchedules
                    .Where(x => x.Destination == item.Value.Destination)
                    .Where(x => x.OrderCount < x.Capacity)
                    .FirstOrDefault();

                if (flight != null)
                {
                    flight.OrderCount++;
                }

                this.Orders.Add(new Order()
                {
                    Name = item.Key,
                    Flight = flight
                });
            }
        }
    }
}
